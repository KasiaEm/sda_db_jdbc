package com.sda.jdbc.common;

public class User {
    private Integer id;
    private String username;
    private String password;
    private Language language;

    public User() {
    }

    public User(Integer id, String username, String password, Language language) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.language = language;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
