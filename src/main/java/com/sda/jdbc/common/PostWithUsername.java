package com.sda.jdbc.common;

public class PostWithUsername {
    private Post post;
    private String username;

    public PostWithUsername() {
    }

    public PostWithUsername(Post post, String username) {
        this.post = post;
        this.username = username;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
