package com.sda.jdbc.common;

public enum Language {
    EN, FR, SP, PL, GE
}
