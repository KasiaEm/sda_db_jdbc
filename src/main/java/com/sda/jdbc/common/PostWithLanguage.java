package com.sda.jdbc.common;

public class PostWithLanguage {
    private Post post;
    private Language language;

    public PostWithLanguage() {
    }

    public PostWithLanguage(Post post, Language language) {
        this.post = post;
        this.language = language;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }
}
