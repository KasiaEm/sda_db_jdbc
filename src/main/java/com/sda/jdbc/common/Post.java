package com.sda.jdbc.common;

import java.time.LocalDateTime;

public class Post {
    private Integer id;
    private String content;
    private LocalDateTime creationDate;
    private Integer userId;

    public Post() {
    }

    public Post(Integer id, String content, LocalDateTime creation_date, Integer userId) {
        this.id = id;
        this.content = content;
        this.creationDate = creation_date;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}

