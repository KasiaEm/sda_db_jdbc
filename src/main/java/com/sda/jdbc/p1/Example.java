package com.sda.jdbc.p1;

import java.sql.*;
import java.util.Properties;

public class Example {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/portal";
    private static final String SQL_INSERT_1 = "INSERT INTO user (username, password, language) VALUES ('john', 'john1', 'EN');";
    private static final String SQL_INSERT_2 = "INSERT INTO user (username, password, language) VALUES ('martyna', 'martyna1', 'PL');";
    private static final String SQL_UPDATE = "UPDATE user SET username = 'john80' WHERE id=1;";
    private static final String SQL_DELETE = "DELETE FROM user WHERE id=2;";
    private static final String SQL_SELECT_BY_LANG = "SELECT id, username, language FROM user WHERE language = 'EN';";

    public static void main(String[] args) {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP: Open a connection
            conn = DriverManager.getConnection(DB_URL, connectionProps);

            //STEP: Execute a query
            stmt = conn.createStatement();
            // insert
            //stmt.executeUpdate(SQL_INSERT_1);
            //stmt.executeUpdate(SQL_INSERT_2);
            // update
            //stmt.executeUpdate(SQL_UPDATE);
            // delete
            //stmt.executeUpdate(SQL_DELETE);
            // select by language
            ResultSet rs = stmt.executeQuery(SQL_SELECT_BY_LANG);

            while (rs.next()) {
                System.out.print(rs.getInt("id"));
                System.out.print(" " + rs.getString("username"));
                System.out.println(" " + rs.getString("language"));
            }
            //STEP: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
