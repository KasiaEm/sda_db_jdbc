package com.sda.jdbc.examples;

import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

public class ExampleSqlInjection {

    public static void main(String[] args) {
        final String DB_URL = "jdbc:mysql://localhost:3306/test";
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = null;
        Statement nothingSuspicious = null;

        try {
            //STEP: Open a connection
            conn = DriverManager.getConnection(DB_URL, connectionProps);

            //STEP: Execute a query
            nothingSuspicious = conn.createStatement();
            String name = new Scanner(System.in).nextLine();
            String searchSql = "SELECT first_name, last_name, age FROM employee WHERE last_name = '" + name + "';";
            ResultSet rs = nothingSuspicious.executeQuery(searchSql);

            while (rs.next()) {
                System.out.print("ID: " + rs.getInt("id"));
                System.out.print(" Age: " + rs.getInt("age"));
                System.out.print(" First: " + rs.getString("first_name"));
                System.out.println(" Last: " + rs.getString("last_name"));
            }
            //STEP: Clean-up environment
            rs.close();
            nothingSuspicious.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (nothingSuspicious != null)
                    nothingSuspicious.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
