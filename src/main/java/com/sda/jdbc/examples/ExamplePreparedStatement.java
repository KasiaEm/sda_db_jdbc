package com.sda.jdbc.examples;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class ExamplePreparedStatement {

    public static void main(String[] args) {
        final String DB_URL = "jdbc:mysql://localhost:3306/test";
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = null;
        PreparedStatement pstmt = null;

        try {
            //STEP: Open a connection
            conn = DriverManager.getConnection(DB_URL, connectionProps);

            //STEP: Execute a query
            pstmt = conn.prepareStatement("SELECT id, first_name, last_name, age FROM employee WHERE last_name = ?");
            pstmt.setString(1, "Doe");
            ResultSet rs = pstmt.executeQuery();

            List<Employee> employees = new LinkedList<>();
            while (rs.next()) {
                System.out.print("ID: " + rs.getInt("id"));
                System.out.print(" Age: " + rs.getInt("age"));
                System.out.print(" First: " + rs.getString("first_name"));
                System.out.println(" Last: " + rs.getString("last_name"));
                int id = rs.getInt("id");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                int age = rs.getInt("age");
                employees.add(new Employee(id, first_name, last_name, age));
            }
            //STEP: Clean-up environment
            rs.close();
            pstmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
