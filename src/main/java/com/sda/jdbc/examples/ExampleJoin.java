package com.sda.jdbc.examples;

import java.sql.*;
import java.util.Properties;

public class ExampleJoin {

    public static void main(String[] args) {
        final String DB_URL = "jdbc:mysql://localhost:3306/test";
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = null;
        Statement stmt = null;

        try {
            //STEP: Open a connection
            conn = DriverManager.getConnection(DB_URL, connectionProps);

            //STEP: Execute a query
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM car JOIN employee ON car.employee_id = employee.id WHERE employee.id = 1");

            while (rs.next()) {
                System.out.print("ID: " + rs.getInt("id"));
                System.out.print(" Age: " + rs.getInt("employee.age"));
                System.out.print(" First: " + rs.getString("employee.first_name"));
                System.out.println(" Last: " + rs.getString("employee.last_name"));
                System.out.println(" Car brand: " + rs.getString("car.brand"));
                System.out.println(" Car plate: " + rs.getString("car.plate_nr"));
            }
            //STEP: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
