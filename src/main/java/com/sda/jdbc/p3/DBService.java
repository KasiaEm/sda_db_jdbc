package com.sda.jdbc.p3;

import com.sda.jdbc.common.*;
import org.apache.commons.codec.digest.DigestUtils;

import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DBService {
    private final Properties connectionProps = new Properties();
    private Connection connection;
    private static final String DB_URL = "jdbc:mysql://localhost:3306/portal";

    public List<User> selectByLanguage(String choosenLanguage) {
        List<User> users = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_SELECT_BY_LANG);
            pstmt.setString(1, choosenLanguage);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String language = rs.getString("language");
                users.add(new User(id, username, null, Language.valueOf(language)));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se2) {
            }
        }
        return users;
    }

    public List<User> selectByUsername(String choosenUsername) {
        List<User> users = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_SELECT_BY_NAME);
            pstmt.setString(1, choosenUsername);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String language = rs.getString("language");
                users.add(new User(id, username, null, Language.valueOf(language)));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return users;
    }

    public void initializeConnection() {
        try {
            FileReader fr = new FileReader(String.valueOf(Paths.get(getClass().getClassLoader().getResource("db.properties").toURI())));
            connectionProps.load(fr);
            fr.close();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
        try {
            connection = DriverManager.getConnection(DB_URL, connectionProps);
            connection.setAutoCommit(false);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public void modifyUser(User user) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_UPDATE);
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getLanguage().name());
            pstmt.setInt(4, user.getId());
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void deleteUser(int id) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_DELETE);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void insertUser(User user) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_INSERT);
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, DigestUtils.md5Hex(user.getPassword()));
            pstmt.setString(3, user.getLanguage().name());
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void insertPost(Post post) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_INSERT);
            pstmt.setString(1, post.getContent());
            pstmt.setString(2, post.getCreationDate().toString());
            pstmt.setInt(3, post.getUserId());
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void modifyPost(Post post) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_UPDATE);
            pstmt.setString(1, post.getContent());
            pstmt.setString(2, post.getCreationDate().toString());
            pstmt.setInt(3, post.getUserId());
            pstmt.setInt(4, post.getId());
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void deletePost(int id) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_DELETE);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public List<Post> selectPostByUser(int choosenUserId) {
        List<Post> posts = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_SELECT_BY_USER);
            pstmt.setInt(1, choosenUserId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String content = rs.getString("content");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime date = LocalDateTime.parse(rs.getString("creation_date"), dtf);
                int userId = rs.getInt("user_id");
                posts.add(new Post(id, content, date, userId));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return posts;
    }

    public List<PostWithLanguage> selectPostByLanguage(Language choosenLanguage) {
        List<PostWithLanguage> postWithLanguage = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_SELECT_BY_LANG);
            pstmt.setString(1, choosenLanguage.name());
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("post.id");
                String content = rs.getString("post.content");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime date = LocalDateTime.parse(rs.getString("post.creation_date"), dtf);
                int userId = rs.getInt("user.id");
                Language language = Language.valueOf(rs.getString("user.language"));
                postWithLanguage.add(new PostWithLanguage(new Post(id, content, date, userId), language));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return postWithLanguage;
    }

    public List<Post> selectPostByDate(LocalDateTime choosenDate) {
        List<Post> posts = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.SQL_POST_SELECT_BY_DATE);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            pstmt.setString(1, choosenDate.format(dtf));
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String content = rs.getString("content");
                LocalDateTime date = LocalDateTime.parse(rs.getString("creation_date"), dtf);
                int userId = rs.getInt("user_id");
                posts.add(new Post(id, content, date, userId));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return posts;
    }

    public List<Post> filterPosts(Integer choosenUserId, Language choosenLanguage, LocalDateTime choosenDate) {
        List<Post> posts = new LinkedList<>();
        PreparedStatement pstmt = null;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String query = choosenLanguage != null ? Queries.SQL_POST_FILTER_JOIN : Queries.SQL_POST_FILTER_NO_JOIN;

        try {
            if (choosenUserId != null) {
                query += Queries.SQL_POST_FILTER_USER;
            }
            if (choosenLanguage != null) {
                query += Queries.SQL_POST_FILTER_LANG;
            }
            if (choosenDate != null) {
                query += Queries.SQL_POST_FILTER_DATE;
            }
            pstmt = connection.prepareStatement(query);

            int counter = 1;
            if (choosenUserId != null) {
                pstmt.setInt(counter++, choosenUserId);
            }
            if (choosenLanguage != null) {
                pstmt.setString(counter++, choosenLanguage.name());
            }
            if (choosenDate != null) {
                pstmt.setString(counter++, choosenDate.format(dtf));
            }
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("post.id");
                String content = rs.getString("post.content");
                LocalDateTime date = LocalDateTime.parse(rs.getString("post.creation_date"), dtf);
                int userId = rs.getInt("post.user_id");
                posts.add(new Post(id, content, date, userId));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return posts;
    }

    public void hashAllPasswords() {
        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(Queries.SQL_USER_SELECT_ALL);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String password = DigestUtils.md5Hex(rs.getString("password"));
                String language = rs.getString("language");
                modifyUser(new User(id, username, password, Language.valueOf(language)));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public User login(String login, String password) {
        String hashPass = DigestUtils.md5Hex(password);
        User user = null;
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(Queries.LOGIN_USER);
            pstmt.setString(1, login);
            pstmt.setString(2, hashPass);
            ResultSet rs = pstmt.executeQuery();

            if(rs.next()) {
                int id = rs.getInt("id");
                String language = rs.getString("language");
                user = new User(id, login, hashPass, Language.valueOf(language));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return user;
    }

    public List<PostWithUsername> seeOthers(int loggedId){
        List<PostWithUsername> others = new LinkedList<>();

        PreparedStatement pstmt = null;
        try {
            pstmt = connection.prepareStatement(Queries.SEE_OTHERS);
            pstmt.setInt(1, loggedId);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("post.id");
                String content = rs.getString("post.content");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                LocalDateTime date = LocalDateTime.parse(rs.getString("post.creation_date"), dtf);
                int userId = rs.getInt("user.id");
                String username = rs.getString("user.username");
                others.add(new PostWithUsername(new Post(id, content, date, userId), username));
            }
            connection.commit();
        } catch (SQLException se) {
            se.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }

        return others;
    }
}
