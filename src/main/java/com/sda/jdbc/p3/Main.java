package com.sda.jdbc.p3;

import com.sda.jdbc.common.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static void main(String[] args) {

        DBService dbService = new DBService();
        dbService.initializeConnection();
        User loggedIn = null;
        //dbService.insertUser( new User(null, "anna", "anna1", Language.PL));

        String line = "";
        while (!line.equals("-exit")) {
            printHelp();
            Scanner sc = new Scanner(System.in);
            line = sc.nextLine();
            if (line.equals("-login")) {
                System.out.println("Login: ");
                String login = new Scanner(System.in).nextLine();
                System.out.println("Password: ");
                String password = new Scanner(System.in).nextLine();
                loggedIn = dbService.login(login, password);
                if (loggedIn != null) {
                    System.out.println("Logged in as: " + loggedIn.getUsername());
                }
            } else if (line.equals("-posts")) {
                if (loggedIn != null) {
                    List<Post> posts = dbService.selectPostByUser(loggedIn.getId());
                    System.out.println("Type -delete to delete, type -back to return.");
                    int i = 0;
                    for (Post p : posts) {
                        System.out.println("[" + i++ + "] " + p.getCreationDate().format(dtf) + " " + p.getContent());
                    }
                    String decision = new Scanner(System.in).nextLine();
                    if (decision.equals("-delete")) {
                        System.out.println("Type post nr to delete...");
                        Integer postToDelete = new Scanner(System.in).nextInt();
                        dbService.deletePost(posts.get(postToDelete).getId());
                    } else if (decision.equals("-back")) {
                        continue;
                    }

                } else {
                    System.out.println("You're not logged in.");
                }
            } else if (line.equals("-logout")) {
                loggedIn = null;
            } else if (line.equals("-add")) {
                if (loggedIn != null) {
                    System.out.println("Type...");
                    String content = new Scanner(System.in).nextLine();
                    dbService.insertPost(new Post(null, content, LocalDateTime.now(), loggedIn.getId()));
                    System.out.println("ADDED.");
                } else {
                    System.out.println("You're not logged in.");
                }
            } else if (line.equals("-others")) {
                if (loggedIn != null) {
                    List<PostWithUsername> posts = dbService.seeOthers(loggedIn.getId());
                    posts.forEach(p -> System.out.println("[" + p.getUsername() + "] " + p.getPost().getCreationDate().format(dtf) + " " + p.getPost().getContent()));
                } else {
                    System.out.println("You're not logged in.");
                }
            }
        }

        dbService.closeConnection();
    }

    public static void printHelp() {
        System.out.println("_____________________ \n"
                + "Type \n"
                + "-exit to exit \n"
                + "-login to login \n"
                + "-posts to show your posts \n"
                + "-add to add new post \n"
                + "-others to see other posts \n"
                + "-logout to logout \n"
                + "_____________________ \n");
    }

    private static void queries() {
        DBService dbService = new DBService();
        dbService.initializeConnection();
        List<User> usersByLang = dbService.selectByLanguage("EN");
        //usersByLang.forEach(u-> System.out.println(u.getUsername()));
        List<User> usersByName = dbService.selectByUsername("martyna");
        //usersByName.forEach(u-> System.out.println(u.getUsername()));
        //dbService.modifyUser(new User(1, "john", "john1328", Language.EN));
        //dbService.deleteUser(3);
        //dbService.insertUser( new User(null, "juan", "dolores", Language.SP));

        LocalDateTime date = LocalDateTime.parse("2019-02-06 12:16:36", dtf);
        //dbService.insertPost(new Post(null, "My first post.", date, 1));
        //dbService.insertPost(new Post(null, "Bla, bla, bla.", date, 2));
        //dbService.modifyPost(new Post(1, "My first post...", date, 1));
        //dbService.deletePost(2);
        List<Post> postsByUser = dbService.selectPostByUser(1);
        //postsByUser.forEach(u-> System.out.println(u.getContent()));
        List<PostWithLanguage> postsByLanguage = dbService.selectPostByLanguage(Language.EN);
        //postsByLanguage.forEach(u-> System.out.println(u.getPost().getContent() + " " + u.getLanguage()));
        LocalDateTime dateFrom = LocalDateTime.parse("2019-01-02 12:16:36", dtf);
        List<Post> postsByDate = dbService.selectPostByDate(dateFrom);
        //postsByDate.forEach(u-> System.out.println(u.getContent() + " " + u.getCreationDate().format(dtf)));
        List<Post> postsFiltered = dbService.filterPosts(null, Language.EN, null);
        //postsFiltered.forEach(u-> System.out.println(u.getContent() + " " + u.getCreationDate().format(dtf)));

        dbService.hashAllPasswords();

        dbService.closeConnection();
    }
}
