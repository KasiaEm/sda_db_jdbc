package com.sda.jdbc.p3;

public class Queries {
    static final String SQL_USER_INSERT = "INSERT INTO user (username, password, language) VALUES (?, ?, ?);";
    static final String SQL_USER_UPDATE = "UPDATE user SET username = ?, password = ?, language = ? WHERE id = ?;";
    static final String SQL_USER_DELETE = "DELETE FROM user WHERE id = ?;";
    static final String SQL_USER_SELECT_BY_LANG = "SELECT id, username, language FROM user WHERE language = ?;";
    static final String SQL_USER_SELECT_BY_NAME = "SELECT id, username, language FROM user WHERE username = ?;";
    static final String SQL_USER_SELECT_ALL = "SELECT * FROM user;";

    static final String SQL_POST_INSERT = "INSERT INTO post (content, creation_date, user_id) VALUES (?, ?, ?);";
    static final String SQL_POST_UPDATE = "UPDATE post SET content = ?, creation_date = ?, user_id = ? WHERE id = ?;";
    static final String SQL_POST_DELETE = "DELETE FROM post WHERE id = ?;";
    static final String SQL_POST_SELECT_BY_USER = "SELECT id, content, creation_date, user_id FROM post WHERE user_id = ?;";
    static final String SQL_POST_SELECT_BY_LANG = "SELECT post.id, post.content, post.creation_date, user.id, user.language FROM post JOIN user ON post.user_id = user.id WHERE user.language = ?;";
    static final String SQL_POST_SELECT_BY_DATE = "SELECT id, content, creation_date, user_id FROM post WHERE creation_date >= ?;";

    static final String SQL_POST_FILTER_NO_JOIN = "SELECT id, content, creation_date, user_id FROM post WHERE true";
    static final String SQL_POST_FILTER_JOIN = "SELECT post.id, post.content, post.creation_date, post.user_id, user.language FROM post JOIN user ON post.user_id = user.id WHERE true";
    static final String SQL_POST_FILTER_USER = " AND post.user_id = ?";
    static final String SQL_POST_FILTER_LANG = " AND user.language = ?";
    static final String SQL_POST_FILTER_DATE = " AND post.creation_date >= ?";

    static final String LOGIN_USER = "SELECT * FROM user WHERE username = ? AND password = ?;";
    static final String SEE_OTHERS = "SELECT * FROM post JOIN user ON post.user_id = user.id WHERE post.user_id <> ?";
}