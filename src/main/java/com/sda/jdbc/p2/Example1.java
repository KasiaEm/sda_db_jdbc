package com.sda.jdbc.p2;

import java.sql.*;
import java.util.Properties;

public class Example1 {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/portal";
    private static final String SQL_SELECT_BY_USERNAME = "SELECT id, username, password, language FROM user WHERE username = '";
    //    private static final String SQL_SELECT_CHECK_PASS_1 = "SELECT id, username, password, language FROM user WHERE username = '";
//    private static final String SQL_SELECT_CHECK_PASS_2 = "' AND password = '";
    private static final String SQL_SELECT_BY_LANG = "SELECT id, username, password, language FROM user WHERE language = '";


    public static void main(String[] args) {
        Properties connectionProps = new Properties();
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        Connection conn = null;
        Statement stmt = null;
        Statement stmt2 = null;

        try {
            //STEP: Open a connection
            conn = DriverManager.getConnection(DB_URL, connectionProps);

            //STEP: Execute a query
            stmt = conn.createStatement();
            stmt2 = conn.createStatement();
            // select by language
            ResultSet rs = stmt.executeQuery(SQL_SELECT_BY_USERNAME + "' or true or '" + "';");


            ResultSet rs2 = stmt.executeQuery(SQL_SELECT_BY_LANG + "EN'; DROP TABLE user; '" +"';");

            showResult(rs);
            showResult(rs2);

            //STEP: Clean-up environment
            rs.close();
            //rs2.close();
            stmt.close();
            stmt2.close();
            conn.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if (stmt2 != null)
                    stmt2.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public static void showResult(ResultSet rs){
        try {
            while (rs.next()) {
                System.out.print(rs.getInt("id"));
                System.out.print(" " + rs.getString("username"));
                System.out.print(" " + rs.getString("password"));
                System.out.println(" " + rs.getString("language"));
            }
            System.out.println("----------------------");
        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}
