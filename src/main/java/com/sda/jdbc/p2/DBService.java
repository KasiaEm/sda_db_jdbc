package com.sda.jdbc.p2;

import com.sda.jdbc.common.Language;
import com.sda.jdbc.common.User;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class DBService {
    private final Properties connectionProps = new Properties();
    private Connection connection;
    private static final String DB_URL = "jdbc:mysql://localhost:3306/portal";
    private static final String SQL_INSERT = "INSERT INTO user (username, password, language) VALUES (?, ?, ?);";
    private static final String SQL_UPDATE = "UPDATE user SET username = ?, password = ?, language = ? WHERE id = ?;";
    private static final String SQL_DELETE = "DELETE FROM user WHERE id = ?;";
    private static final String SQL_SELECT_BY_LANG = "SELECT id, username, language FROM user WHERE language = ?;";
    private static final String SQL_SELECT_BY_NAME = "SELECT id, username, language FROM user WHERE username = ?;";

    public List<User> selectByLanguage(String choosenLanguage){
        List<User> users = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(SQL_SELECT_BY_LANG);
            pstmt.setString(1, choosenLanguage);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String language = rs.getString("language");
                users.add(new User(id, username, null, Language.valueOf(language)));
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se2) {
            }
        }
        return users;
    }

    public List<User> selectByUsername(String choosenUsername){
        List<User> users = new LinkedList<>();
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement("SELECT id, username, language FROM user WHERE username = ?;");
            pstmt.setString(1, choosenUsername);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                String language = rs.getString("language");
                users.add(new User(id, username, null, Language.valueOf(language)));
            }
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        return users;
    }

    public void initializeConnection() {
        connectionProps.put("user", "root");
        connectionProps.put("password", "root");
        connectionProps.put("serverTimezone", "CET");

        try {
            connection = DriverManager.getConnection(DB_URL, connectionProps);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection(){
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException se) {
            se.printStackTrace();
        }
    }

    public void modifyUser(User user) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(SQL_UPDATE);
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2,user.getPassword());
            pstmt.setString(3, user.getLanguage().name());
            pstmt.setInt(4, user.getId());
            pstmt.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void deleteUser(int id) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(SQL_DELETE);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public void insertUser(User user) {
        PreparedStatement pstmt = null;

        try {
            pstmt = connection.prepareStatement(SQL_INSERT);
            pstmt.setString(1, user.getUsername());
            pstmt.setString(2, user.getPassword());
            pstmt.setString(3, user.getLanguage().name());
            pstmt.executeUpdate();
        } catch (SQLException se) {
            se.printStackTrace();
        } finally {
            try {
                if (pstmt != null)
                    pstmt.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
