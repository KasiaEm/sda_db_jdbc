package com.sda.jdbc.p2;

import com.sda.jdbc.common.Language;
import com.sda.jdbc.common.User;

import java.sql.*;
import java.util.List;
import java.util.Properties;

public class Example2 {

    public static void main(String[] args) {
        DBService dbService = new DBService();
        dbService.initializeConnection();
        List<User> usersByLang = dbService.selectByLanguage("EN");
        usersByLang.forEach(u-> System.out.println(u.getUsername()));
        List<User> usersByName = dbService.selectByUsername("martyna");
        usersByName.forEach(u-> System.out.println(u.getUsername()));
        //dbService.modifyUser(new User(1, "john", "john1328", Language.EN));
        //dbService.deleteUser(3);
        //dbService.insertUser( new User(null, "juan", "dolores", Language.SP));
        dbService.closeConnection();
    }
}
